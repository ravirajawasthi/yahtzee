import React, { Component } from "react";
import "./Die.css";

class Die extends Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick() {
    this.props.handleClick(this.props.idx)
  }
  render() {
    
    let numtoword = ['one', 'two','three','four','five','six'];
    let classes = `Die fas fa-dice-${numtoword[this.props.val - 1]} fa-5x ${this.props.locked ? "Die-locked" : ""} ${this.props.rolling?"Die-rolling":""}`;
    return (
      <i
        className={classes}
        onClick={this.handleClick}
      >
      </i>
    );
  }
}

export default Die;
