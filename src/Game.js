import React, { Component } from "react";
import Dice from "./Dice";
import ScoreTable from "./ScoreTable";
import "./Game.css";

const NUM_DICE = 5;
const NUM_ROLLS = 3;

class Game extends Component {
  static defaultProps = {
    messages : ["Last roll","2 Rerolls left","Starting roll"]
  }
  constructor(props) { 
    super(props);
    this.state = {
      dice: Array.from({ length: NUM_DICE }).fill(1),
      locked: Array(NUM_DICE).fill(false),
      rollsLeft: NUM_ROLLS,
      rolling: false,
      firstRoll: true,
      scores: {
        ones: undefined,
        twos: undefined,
        threes: undefined,
        fours: undefined,
        fives: undefined,
        sixes: undefined,
        threeOfKind: undefined,
        fourOfKind: undefined,
        fullHouse: undefined,
        smallStraight: undefined,
        largeStraight: undefined,
        yahtzee: undefined,
        chance: undefined
      }
    };
    this.roll = this.roll.bind(this);
    this.doScore = this.doScore.bind(this);
    this.toggleLocked = this.toggleLocked.bind(this);
    this.doScore = this.doScore.bind(this);
    this.assignRandNum = this.assignRandNum.bind(this);
    this.getTotalScore = this.getTotalScore.bind(this);
  }

  getTotalScore(){
    let totalScore = 0;
    let scores = this.state.scores
    for(let i in scores){
      if (scores[i]) totalScore += scores[i];
    }
    return totalScore
  }

  roll(evt) {
    this.setState({rolling: true, firstRoll: false}, () =>{setTimeout(this.assignRandNum, 1000)});
  }

  assignRandNum(evt){
    // roll dice whose indexes are in reroll and assign them numbers
    this.setState(st => ({
      dice: st.dice.map((d, i) =>
        st.locked[i] ? d : Math.ceil(Math.random() * 6)
      ),
      locked: st.rollsLeft > 1 ? st.locked : Array(NUM_DICE).fill(true),
      rollsLeft: st.rollsLeft - 1,
      rolling: false
    }));
  }

  toggleLocked(idx) {
    // toggle whether idx is in locked or not
    if (this.state.rollsLeft > 0) {
      this.setState(st => ({
        locked: [
          ...st.locked.slice(0, idx),
          !st.locked[idx],
          ...st.locked.slice(idx + 1)
        ]
      }));
    }
  }

  doScore(rulename, ruleFn) {
    // evaluate this ruleFn with the dice and score this rulename
    this.setState(st => ({
      scores: { ...st.scores, [rulename]: ruleFn(this.state.dice) },
      rollsLeft: NUM_ROLLS,
      locked: Array(NUM_DICE).fill(false)
    }));
    this.roll();
  }

  render() {
    return (
      <div className='Game Game-score-section'>
        <header className='Game-header'>
          <h1 className='App-title'>Yahtzee!</h1>

          <section className='Game-dice-section'>
            <Dice
              rolling={this.state.rolling}
              dice={this.state.dice}
              locked={this.state.locked}
              handleClick={this.toggleLocked}
            />
            <div className='Game-button-wrapper'>
              <button
                className='Game-reroll'
                disabled={this.state.locked.every(x => x) || this.state.rollsLeft === 0 || this.state.rolling === true}
                onClick={this.roll}
              >
                {this.props.messages[this.state.rollsLeft - 1]?this.props.messages[this.state.rollsLeft - 1]:"No more rolls"}
              </button>
            </div>
          </section>
        </header>
        
        {
          !this.state.firstRoll ?
            <div><ScoreTable doScore={this.doScore} scores={this.state.scores} firstRoll = {this.state.firstRoll}/>
            <h2>Total Score : {this.getTotalScore()}</h2></div>
          :
        <h1>Let's Begin</h1>
        }
          
      </div>
    );
  }
}

export default Game;
